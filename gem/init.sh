#! /bin/bash

mkdir -p ${HOME}/user/gem

if [ -e ${HOME}/user/gem/package-list ]
then
  gem install $(cat ${HOME}/user/gem/package-list)
fi
