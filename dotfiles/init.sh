#! /bin/bash

if [ -e ${HOME}/user/dotfiles/package-list ]
then
    dotfiles install $(cat ${HOME}/user/dotfiles/package-list | tr '\n' ' ')
fi
