#! /bin/bash

root=$(cat ${HOME}/.backup-packages/repository)

# Update the status
for package in $(${HOME}/bin/backup-packages list)
do
    if [ -e ${root}/${package}/"update.sh" ]
    then
        ${root}/${package}/update.sh
    fi
done
