#! /bin/bash

mkdir -p ${HOME}/user/npm

if [ -e ${HOME}/user/npm/global-package-list ]
then
  for x in $(cat ${HOME}/user/npm/global-package-list)
  do
    sudo npm -g install $i
  done
fi
