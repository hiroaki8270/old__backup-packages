#! /bin/bash

mkdir -p ${HOME}/user/npm

npm -g ls --depth=0 2> /dev/null | awk '{print $2}'| sed -e '1,1d' | cut -f 1 -d '@' | grep -v '(empty)' > ${HOME}/user/npm/global-package-list
