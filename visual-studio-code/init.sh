#! /bin/bash

mkdir -p ${HOME}/user/visual-studio-code
if [ -e ${HOME}/user/visual-studio-code/package-list ]
then
    for x in $(cat ${HOME}/user/visual-studio-code/package-list)
    do
        code --install-extension ${x}
    done
fi