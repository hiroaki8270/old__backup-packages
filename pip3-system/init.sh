#! /bin/bash

mkdir -p ${HOME}/user/pip3

if [ -e ${HOME}/user/pip3/global-package-list ]
then
  for x in $(cat ${HOME}/user/pip3/global-package-list)
  do
    sudo pip3 install $i
  done
fi