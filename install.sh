#! /bin/bash

mkdir -p ${HOME}/bin
ln -sf $(realpath $(dirname $0))/backup-packages ${HOME}/bin/

mkdir -p ${HOME}/.backup-packages
echo $(realpath $(dirname $0)) > ${HOME}/.backup-packages/repository
