#! /bin/bash

mkdir -p ${HOME}/.ssh
mkdir -p ${HOME}/user/openssh/.ssh
for file in $(find ${HOME}/user/openssh/.ssh/ -type f)
do
    ln -sf $(realpath $file) ${HOME}/.ssh
done