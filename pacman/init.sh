#! /bin/bash

mkdir -p ${HOME}/user/pacman/
if [ -e ${HOME}/user/pacman/package-list ]
then
  for x in $(cat ${HOME}/user/pacman/package-list)
  do
    if [ $(pacman -Qq | grep -x -i $x) ]
    then
      echo $x is Already installed
    else
      pacman -S $x
    fi
  done
fi
