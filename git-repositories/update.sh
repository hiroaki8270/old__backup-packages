#! /bin/bash

mkdir -p ${HOME}/user/git-repositories
for repository in $(find ${HOME} -name ".git" -exec dirname {} \; | sed -e "s|^${HOME}/||g")
do
    if git -C ${HOME}/${repository} remote | grep "^origin$"
    then
        # There is a remote repository: "origin"
        mkdir -p ${HOME}/user/git-repositories/${repository}
        git -C ${HOME}/${repository} remote show origin | grep "Fetch URL:" | cut -f 5 -d " " > ${HOME}/user/git-repositories/${repository}/url
        cp ${HOME}/${repository}/.git/config ${HOME}/user/git-repositories/${repository}/config
    fi
done