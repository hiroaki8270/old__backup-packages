#! /bin/bash

mkdir -p ${HOME}/user/git-repositories
for repository in $(find ${HOME}/user/git-repositories -name "url" -exec dirname {} \; | sed -e "s|^${HOME}/user/git-repositories/||g")
do
    mkkdir $(dirname ${HOME}/${repository})
    if [ -e ${HOME}/${repository} ]
    then
        echo "${HOME}/${repository} already exists."
    else
        git clone $(cat ${HOME}/user/git-repositories/${repository}/url) ${HOME}/${repository}
        cp ${HOME}/user/git-repositories/${repository}/config ${HOME}/${repository}/.git/config
    fi
done