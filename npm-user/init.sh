#! /bin/bash

mkdir -p ${HOME}/user/npm

cd ${HOME}
if [ -e ${HOME}/user/npm/package-list ]
then
  for x in $(cat ${HOME}/user/npm/package-list)
  do
    npm install $i
  done
fi