#! /bin/bash

mkdir -p ${HOME}/user/pip3

cd ${HOME}
pip3 list --user --format columns | sed -e '1,2d' | cut -f 1 -d " " > ${HOME}/user/pip3/package-list
